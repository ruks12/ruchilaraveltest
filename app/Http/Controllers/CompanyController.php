<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyStoreRequest;
use Config;
use Validator,Redirect,Response,File;

use DataTables;
use App\Mail\SendWelcomeEmail;
use Mail;

class CompanyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = Company::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('logo', function ($data) { 
                        $url=asset("$data->logo"); 
                        return $url; 
                    })

                    
                    ->addColumn('action', function($data){
   

                           $btn = '<a href="/companies/'.$data->id.'" class="edit btn btn-primary btn-sm">View</a>  <a href="/companies/'.$data->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a> <button class="delete btn btn-danger btn-sm" data-remote='.$data->id.'>Delete</button>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('companies.index');

/*        $companies = Company::latest()->paginate(5);
  
        return view('companies.index',compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * 5);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        $validated = $request->validated();

        if ($files = $request->file('logo')) {

            $fileName = time().'_'.$request->logo->getClientOriginalName();
            $file = $request->file('logo')->storeAs('uploads', $fileName, 'public');
            //$file = $request->logo->store('public');

                    // Save In Database
             $companymodel= new Company();
             $companymodel->name=$request->name;
             $companymodel->email=$request->email;
             $companymodel->logo='/storage/' .$file;
             $companymodel->website=$request->website;
             if($companymodel->save()){


                $mailData = [];
                $mailData['name'] = $request->name;
                $mailData['email'] = $request->email;
                $mailData['subject'] = 'Welcome to ibees';
                Mail::to('anubhav@test.com')->send(new SendWelcomeEmail($mailData));
            //Company::create($request->all());

                return redirect()->route('companies.index')
                            ->with('success','Company created successfully.');
            }      

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('companies.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('companies.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {

        if ($files = $request->file('logo')) {

            $fileName = time().'_'.$request->logo->getClientOriginalName();
            $file = $request->file('logo')->storeAs('uploads', $fileName, 'public');
            $file = '/storage/' .$file;
            $company->logo=$file;
        } 
            
            $company->name=$request->name;
            $company->email=$request->email;          
            $company->website=$request->website;
            if($company->update()){
                return redirect()->route('companies.index')
                        ->with('success','Company updated successfully');
            }
        
    }

  /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Company::findOrFail($id);
        $data->delete();
        return redirect()->route('companies.index')
                        ->with('success','Company Delete successfully');
    }

}
