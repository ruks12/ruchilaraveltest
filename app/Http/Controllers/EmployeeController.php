<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeStoreRequest;
use Config;
use Validator,Redirect,Response,File;
use DataTables;

class EmployeeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if ($request->ajax()) {
            $data = Employee::select(
                            "employees.id", 
                            "employees.firstname",
                            "employees.lastname",
                            "employees.email",
                            "employees.phone",
                            "companies.name as company"
                        )
                        ->join("companies", "companies.id", "=", "employees.company")
                        ->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($data){
   

                           $btn = '<a href="/employees/'.$data->id.'" class="edit btn btn-primary btn-sm">View</a>  <a href="/employees/'.$data->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a> <button class="delete btn btn-danger btn-sm" data-remote='.$data->id.'>Delete</button>';
     
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                $companies = Company::select('id', 'name')->get();

        return view('employees.create')->with('companies', $companies);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            Employee::create($request->all());

                return redirect()->route('employees.index')
                            ->with('success','Employee created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.show',compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit',compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
            $employee->firstname=$request->firstname;
            $employee->lastname=$request->lastname;
            $employee->email=$request->email;
            $employee->phone=$request->phone;          
           
            if($employee->update()){
                return redirect()->route('employees.index')
                        ->with('success','employee updated successfully');
            }
    }

  /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Employee::findOrFail($id);
        $data->delete();
        return redirect()->route('employees.index')
                        ->with('success','Employee Delete successfully');
    }
}
