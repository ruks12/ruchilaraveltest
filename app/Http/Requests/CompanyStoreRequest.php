<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CompanyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:companies',
            'name' => 'required|string|max:50',
            'logo' => 'required|dimensions:min_width=100,min_height=100',
            'website'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'name.required' => 'Name is required!',
            'logo.required' => 'Logo is required! ',
            'logo.dimensions' => 'Image size min 100*100',
            'website.required'=>'Website is required'
        ];
    }
}
