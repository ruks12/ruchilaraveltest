<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string|max:50',
            'lastname' => 'required|string|max:50',
            'company' =>'required|not_in:0',
            'email' => 'required|email|unique:employees',
            'mobile' =>'required|size:11|numeric',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'firstname.required' => 'FirstName is required!',
            'lastname.required' => 'LastName is required!',
            'company.required' => 'CompanyName is required!',
            'mobile.required' => 'Mobile is required! ',
            'mobile.size' => 'Mobile minimum 10 digits! ',
        ];
    }
}
