@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
    
    <a href="{{ LaravelLocalization::localizeUrl('/home') }}">@lang('Follow this link')</a>

                    <li><a href="/home" class="nav-link {{ request()->is('home*') ? 'active' : '' }}">Home</a></li>
                    <li><a href="/employees" class="nav-link {{ request()->is('employees*') ? 'active' : '' }}">Employees</a></li>
                    <li><a href="/companies" class="nav-link {{ request()->is('companies*') ? 'active' : '' }}">Company</a></li>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
